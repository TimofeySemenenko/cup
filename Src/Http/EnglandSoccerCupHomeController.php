<?php declare(strict_types=1);


namespace EnglandSoccerCup\Http;

use App\Http\Controllers\Controller;

/**
 * Class EnglandSoccerCupHomeController
 * @package App\Isp\Http\Controllers
 */
class EnglandSoccerCupHomeController extends Controller
{}

